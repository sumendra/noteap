package com.example.noteap;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.noteap.Db.NoteHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.example.noteap.Db.DatabaseContract.NoteColumns.CONTENT_URI;
import static com.example.noteap.Db.DatabaseContract.NoteColumns.DATE;
import static com.example.noteap.Db.DatabaseContract.NoteColumns.DESCRIPTION;
import static com.example.noteap.Db.DatabaseContract.NoteColumns.TITLE;

public class NoteAddUpdateActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText inputTitle;
    private EditText inputDeskription;
    private Button buttonSubmit;

    public static final String EXTRA_NOTE="extra_note";
    public static final String EXTRA_POSITION="extra_position";

    private boolean edit= false;
    public static final int REQUEST_ADD= 100;
    public static final int RESULT_ADD= 101;
    public static final int REQUEST_UPDATE= 200;
    public static final int RESULT_UPDATE= 201;
    public static final int RESULT_DELETE= 301;

    private Note note;
    private int position;

    private NoteHelper noteHelper;

    String actionBarTitle;
    String buttonTitle;

    private final int ALERT_DIALOG_CLOSE= 10;
    private final int ALERT_DIALOG_DELETE= 20;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_add_update);

        inputTitle= findViewById(R.id.input_title);
        inputDeskription= findViewById(R.id.input_deskription);
        buttonSubmit= findViewById(R.id.button_submit);
        buttonSubmit.setOnClickListener(this);
        noteHelper= NoteHelper.getInstance(getApplicationContext());
        // get note
        note= getIntent().getParcelableExtra(EXTRA_NOTE);
        // if note null
        if(note!= null){
            // get extra position
            position= getIntent().getIntExtra(EXTRA_POSITION, 0);
            // set edit true
            edit= true;
        }
        else {
            // make new note
            note= new Note();
        }

        Uri uri= getIntent().getData();
        if (uri != null){
            Cursor cursor= getContentResolver().query(uri, null, null, null, null);
            if(cursor!=null){
                if (cursor.moveToFirst()) note = new Note(cursor);
                cursor.close();
            }
        }

        // if edit true
        if (edit){
            // set action bar ubah
            actionBarTitle= "Ubah";
            // button update
            buttonTitle= "Update";
            if(note!= null){
                // settext editext
                inputTitle.setText(note.getTitle());
                inputDeskription.setText(note.getDescription());
            }
        }

        // else
        else {
            // set action title tambah
            actionBarTitle="Tambah";
            // set button simpan
            buttonTitle= "Simpan";
        }


        // set actionbar
        if(getSupportActionBar() != null){
            // set title action bar
            getSupportActionBar().setTitle(actionBarTitle);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
        }

        // settext button
        buttonSubmit.setText(buttonTitle);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.button_submit){
            String title= inputTitle.getText().toString().trim();
            String deskription= inputDeskription.getText().toString().trim();
            if(TextUtils.isEmpty(title)){
                inputTitle.setError("Field can not be blank");
                return;
            }
            // set value note from edittext
            note.setTitle(title);
            note.setDescription(deskription);

            // intent to main
            Intent intent= new Intent();
            // give note to send main activity
            intent.putExtra(EXTRA_NOTE, note);
            // give position to send main activity
            intent.putExtra(EXTRA_POSITION, position);

            // jika note tidak null

            ContentValues values= new ContentValues();
            values.put(TITLE, title);
            values.put(DESCRIPTION, deskription);
            if (edit){
                getContentResolver().update(getIntent().getData(), values, null, null);
                Toast.makeText(this, "Satu item berhasil di update ", Toast.LENGTH_SHORT).show();
                finish();
            }
            else {
                values.put(DATE, getCurrentDate());
                note.setDate(getCurrentDate());
                Toast.makeText(this, "Satu item berhasil di tambah", Toast.LENGTH_SHORT).show();
                getContentResolver().insert(CONTENT_URI, values);
                finish();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (edit){
            getMenuInflater().inflate(R.menu.menu_form, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_delete:
                // show alert if will delete
                showAlertDialog(ALERT_DIALOG_DELETE);
                break;
            case android.R.id.home:
                // set show alert if can back to main
                showAlertDialog(ALERT_DIALOG_CLOSE);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void showAlertDialog(int type){
        final boolean dialogClose= type == ALERT_DIALOG_CLOSE;
        String dialogTitle, dialogMessage;

        // if ALERT DIALOG CLOSE
        if(dialogClose){
            dialogTitle= "Batal";
            dialogMessage= "Apoakah anda ingin membatalkan perubahan pada form?";
        }
        else{
            dialogMessage= "Apakah anda yakin ingin menghapus item ini?";
            dialogTitle= "Hapus Note";
        }

        AlertDialog.Builder alertDialogBuilder= new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(dialogTitle);
        alertDialogBuilder
                .setMessage(dialogMessage)
                .setCancelable(false)
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (dialogClose){
                            finish();
                        }
                        else{
                            Intent intent= new Intent();
                            intent.putExtra(EXTRA_POSITION, position);
                            Toast.makeText(NoteAddUpdateActivity.this, "Satu item berhasil di hapus", Toast.LENGTH_SHORT).show();
                            getContentResolver().delete(getIntent().getData(), null, null);
                            finish();
                        }
                    }
                })
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog= alertDialogBuilder.create();
        alertDialog.show();
    }


    // call current date
    private String getCurrentDate(){
        DateFormat dateFormat= new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault());
        Date date= new Date();
        return dateFormat.format(date);
    }
}
