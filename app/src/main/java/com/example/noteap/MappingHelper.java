package com.example.noteap;


import android.database.Cursor;

import java.util.ArrayList;

import static android.provider.BaseColumns._ID;
import static com.example.noteap.Db.DatabaseContract.NoteColumns.DATE;
import static com.example.noteap.Db.DatabaseContract.NoteColumns.DESCRIPTION;
import static com.example.noteap.Db.DatabaseContract.NoteColumns.TITLE;

// mengubah nilai kembalian dari cursor ke arraylist
public class MappingHelper {
    public static ArrayList<Note> mapCursorArrayList (Cursor cursor){
        ArrayList<Note> notelist= new ArrayList<>();
        while (cursor.moveToNext()){
            int id=  cursor.getInt(cursor.getColumnIndexOrThrow(_ID));
            String title= cursor.getString(cursor.getColumnIndexOrThrow(TITLE));
            String deskripsi= cursor.getString(cursor.getColumnIndexOrThrow(DESCRIPTION));
            String date= cursor.getString(cursor.getColumnIndexOrThrow(DATE));
            notelist.add(new Note(id, title, deskripsi, date));

        }
        return notelist;
    }
}
