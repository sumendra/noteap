package com.example.noteap;

import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.noteap.Db.NoteHelper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import static com.example.noteap.Db.DatabaseContract.NoteColumns.CONTENT_URI;
import static com.example.noteap.MappingHelper.mapCursorArrayList;
import static com.example.noteap.NoteAddUpdateActivity.REQUEST_UPDATE;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, LoadNotesCallback {


    // make variable recyclerview
    private RecyclerView recyclerNote;
    private ProgressBar progressBar;
    private FloatingActionButton fabPluss;
    private static final String EXTRA_STATE= "EXTRA_STATE";
    private NodeAdapter adapter;
    private static HandlerThread handlerThread;
    private NoteHelper noteHelper;
    private DataObserver observer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // set action bar
        if(getSupportActionBar() != null)
            getSupportActionBar().setTitle("Notes");
        recyclerNote= findViewById(R.id.recycler_note);
        recyclerNote.setLayoutManager(new LinearLayoutManager(this));
        recyclerNote.setHasFixedSize(true);
        noteHelper= NoteHelper.getInstance(getApplicationContext());
        noteHelper.open();
        progressBar= findViewById(R.id.progressBar);
        fabPluss= findViewById(R.id.fab_pluss);
        fabPluss.setOnClickListener(this);
        adapter= new NodeAdapter(this);
        recyclerNote.setAdapter(adapter);
        if(savedInstanceState==null){
            new LoadNoteAsync(this, this).execute();
        }
        else {
            ArrayList<Note> list= savedInstanceState.getParcelableArrayList(EXTRA_STATE);
            if(list!= null){
                adapter.setListNotes(list);
            }
        }
        handlerThread= new HandlerThread("DataObserver");
        handlerThread.start();
        Handler handler= new Handler(handlerThread.getLooper());
        observer= new DataObserver(handler, this);
        getContentResolver().registerContentObserver(CONTENT_URI, true, observer);
        if(savedInstanceState == null){
            new LoadNoteAsync(this, this).execute();
        }
        else {
            ArrayList<Note> list= savedInstanceState.getParcelableArrayList(EXTRA_STATE);
            if(list!= null){
                adapter.setListNotes(list);
            }
        }

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState, @NonNull PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putParcelableArrayList(EXTRA_STATE, adapter.getListNotes());
    }

    @Override
    public void onClick(View view) {

        // ketika fab diklik
        if(view.getId() == R.id.fab_pluss){
            // intent to noteAddUpdateActivity
            Intent intent= new Intent(MainActivity.this, NoteAddUpdateActivity.class);
            startActivityForResult(intent, NoteAddUpdateActivity.REQUEST_ADD);
        }
    }

    // tampilkan progress bar ketika proses data
    @Override
    public void preExecute() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.VISIBLE);
            }
        });
    }

    // ketika data disimpan
    @Override
    public void postExecute(Cursor note) {
        // sembunyikan progress bar
        progressBar.setVisibility(View.INVISIBLE);
        // mengatur value list note
        ArrayList<Note> list= mapCursorArrayList(note);
        if(list.size()>0) {
            adapter.setListNotes(list);
        }
        else{
            adapter.setListNotes(new ArrayList<Note>());
            showSnackbarMessage("Tidak ada data saat ini");
        }
    }

    // berfungsi untuk menjalankan suatu data secara asyncrounus
    private static class LoadNoteAsync extends AsyncTask<Void, Void, Cursor>{

        private final WeakReference<Context> Prefcontext;
        private final WeakReference<LoadNotesCallback> weakCallback;
        private LoadNoteAsync(Context context, LoadNotesCallback callback){
            Prefcontext= new WeakReference<>(context);
            weakCallback= new WeakReference<>(callback);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            weakCallback.get().preExecute();
        }

        @Override
        protected Cursor doInBackground(Void... voids) {
            Context context= Prefcontext.get();
            return context.getContentResolver().query(CONTENT_URI, null, null, null, null);
        }

        @Override
        protected void onPostExecute(Cursor notes) {
            super.onPostExecute(notes);
            weakCallback.get().postExecute(notes);
        }
    }


    // melakukan seleksi berdasarkan request_code dari data yang dikirim oleh activity note
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        // REQUEST dikirim dari activity note

        // ketika data tidak kosong
        if (data!= null){

            // ketika request code yang dikirim activity note
            if (requestCode==NoteAddUpdateActivity.REQUEST_ADD){
                Note note= data.getParcelableExtra(NoteAddUpdateActivity.EXTRA_NOTE);
                adapter.addItem(note);
                recyclerNote.smoothScrollToPosition(adapter.getItemCount()-1);
                showSnackbarMessage("Satu item berhasil ditambahkan");
            }
            // ketika request code REQUEST_UPDATE
            else if (requestCode == REQUEST_UPDATE){

                if(resultCode== NoteAddUpdateActivity.RESULT_UPDATE){
                    Note note= data.getParcelableExtra(NoteAddUpdateActivity.EXTRA_NOTE);
                    int position= data.getIntExtra(NoteAddUpdateActivity.EXTRA_POSITION, 0);
                    // ubah adapter ke data yang paling terbaru yang telah di update

                    adapter.updateItem(position, note);
                    recyclerNote.smoothScrollToPosition(position);
                    showSnackbarMessage("Satu item berhasil diubah");
                }
                // ketika status delete
                else if (resultCode== NoteAddUpdateActivity.RESULT_DELETE){
                    int position= data.getIntExtra(NoteAddUpdateActivity.EXTRA_POSITION,0);
                    // hapus data dari adapter
                    adapter.removeItem(position);
                    showSnackbarMessage("Satu item berhasil dihapus");
                }
            }
        }
    }


    // menampilkan snackbar
    private void showSnackbarMessage(String message){
        Snackbar.make(recyclerNote, message, Snackbar.LENGTH_SHORT). show();
    }


    // ketika aplikasi di tutup akan menutup helper
    @Override
    protected void onDestroy() {
        super.onDestroy();
        noteHelper.close();
    }
    public static class DataObserver extends ContentObserver{
        final Context context;
        public DataObserver (Handler handler, Context context){
            super(handler);
            this.context= context;
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            new LoadNoteAsync(context, (LoadNotesCallback)context).execute();
        }
    }
}
