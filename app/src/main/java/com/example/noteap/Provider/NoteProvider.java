package com.example.noteap.Provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.noteap.Db.NoteHelper;
import com.example.noteap.MainActivity;

import static com.example.noteap.Db.DatabaseContract.AUTHORITY;
import static com.example.noteap.Db.DatabaseContract.NoteColumns.CONTENT_URI;
import static com.example.noteap.Db.DatabaseContract.TABLE_NOTE;

public class NoteProvider extends ContentProvider {

    private static final int NOTE=1;
    private static final int NOTE_ID= 2;
    private static final UriMatcher ur= new UriMatcher(UriMatcher.NO_MATCH);
    private NoteHelper noteHelper;
    int updated;

    static{
        ur.addURI(AUTHORITY, TABLE_NOTE, NOTE);
        ur.addURI(AUTHORITY, TABLE_NOTE + "/#", NOTE_ID);
    }
    @Override
    public boolean onCreate() {
        noteHelper= NoteHelper.getInstance(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        noteHelper.open();
        Cursor cursor;
        switch (ur.match(uri)){
            case NOTE:
                cursor= noteHelper.queryProvider();
                break;
            case NOTE_ID:
                cursor= noteHelper.queryByIdProvider(uri.getLastPathSegment());
                break;
                default:
                    cursor=null;
                    break;
        }
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        noteHelper.open();
        long added;
        switch (ur.match(uri)){
            case NOTE:
                added= noteHelper.insertProvider(values);
                break;
                default:
                    added=0;
                    break;
        }
        getContext().getContentResolver().notifyChange(CONTENT_URI, new MainActivity.DataObserver(new Handler(), getContext()));
        return Uri.parse(CONTENT_URI + "/"+ added);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        noteHelper.open();
        int deleted;
        switch (ur.match(uri)){
            case NOTE_ID:
                deleted= noteHelper.deleteProvider(uri.getLastPathSegment());
                break;
                default:
                    deleted=0;
                    break;
        }
        getContext().getContentResolver().notifyChange(CONTENT_URI, new MainActivity.DataObserver(new Handler(), getContext()));
        return deleted;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        noteHelper.open();
        switch (ur.match(uri)){
            case NOTE_ID:
                updated= noteHelper.updateProvider(uri.getLastPathSegment(), values);
                break;
            default:
                updated=0;
                break;
        }
        getContext().getContentResolver().notifyChange(CONTENT_URI, new MainActivity.DataObserver(new Handler(), getContext()));
        return updated;
    }
}
