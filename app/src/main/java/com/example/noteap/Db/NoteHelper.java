package com.example.noteap.Db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.noteap.Note;

import java.util.ArrayList;

import static android.provider.BaseColumns._ID;
import static com.example.noteap.Db.DatabaseContract.NoteColumns.DATE;
import static com.example.noteap.Db.DatabaseContract.NoteColumns.DESCRIPTION;
import static com.example.noteap.Db.DatabaseContract.NoteColumns.TITLE;
import static com.example.noteap.Db.DatabaseContract.TABLE_NOTE;


// fungsi digunakan singleton pattern agar suatu constructor tidak dapat dipanggil sembarangan dan tidak berjalan jika tidak dibutuhkan
public class NoteHelper {
    private static final String DATABASE_TABLE= TABLE_NOTE;
    private static DatabaseHelper databaseHelper;
    private static NoteHelper INSTANCE;
    private static SQLiteDatabase database;
    private NoteHelper (Context context){
        databaseHelper= new DatabaseHelper(context);
    }


    // menggunakan singleton pattern untuk dapat membuat satu instance
    public static NoteHelper getInstance(Context context){
        // dengan menggunakan singleton pattern kelas tidak dapat dinstance secara sembarang dengan bantuan new constructor
        if(INSTANCE == null){
            // melakukan pengecekan apakah instance null
            synchronized (SQLiteOpenHelper.class){
                if(INSTANCE == null){
                    // jika null buat instance baru
                    INSTANCE= new NoteHelper(context);
                }
            }
        }
        return INSTANCE;
    }

    public void open() throws SQLException{
        database= databaseHelper.getWritableDatabase();
    }
    public void close(){
        databaseHelper.close();
        if(database.isOpen()){
            database.close();
        }
    }


    // berfungsi untuk mengambil data dari database
     public ArrayList<Note> getAllNotes(){
        ArrayList<Note> arrayList= new ArrayList<>();
        Cursor cursor= database.query(DATABASE_TABLE, null,
                null,
                null,
                null,
                null,
                _ID + " ASC ",
                null);
        cursor.moveToFirst();
        Note note;
        if(cursor.getCount()>0){
            do {
                note= new Note();
                note.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
                note.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(TITLE)));
                note.setDescription(cursor.getString(cursor.getColumnIndexOrThrow(DESCRIPTION)));
                note.setDate(cursor.getString(cursor.getColumnIndexOrThrow(DATE)));

                // memasukkan data ke dalam arraylist
                arrayList.add(note);
                cursor.moveToNext();
            } while (!cursor.isAfterLast());
        }

        cursor.close();
        // mengembalikan nilai arraylist
        return arrayList;
    }

    public long insertNote(Note note){
        ContentValues args= new ContentValues();
        args.put(TITLE, note.getTitle());
        args.put(DESCRIPTION, note.getDescription());
        args.put(DATE, note.getDate());
        return database.insert(DATABASE_TABLE, null, args);
    }


    // update data ke dalam database
    public int updateNote(Note note){
        ContentValues args= new ContentValues();
        args.put(TITLE, note.getTitle());
        args.put(DESCRIPTION, note.getDescription());
        args.put(DATE, note.getDate());
        return database.update(DATABASE_TABLE, args, _ID + "= '"+ note.getId()+"'", null);
    }


    // delete data dari database
    public int deleteNote(int id){
        return database.delete(TABLE_NOTE, _ID+ " = '"+id+"'", null);
    }

    public Cursor queryByIdProvider(String id){
        return database.query(DATABASE_TABLE, null
        , _ID + " = ? "
        , new String[]{}
        , null
        , null
        , null
        , null);
    }
    public Cursor queryProvider(){
        return database.query(DATABASE_TABLE
        ,null
        ,null
        ,null
        ,null
        ,null
        ,_ID + " ASC ");
    }
    public long insertProvider(ContentValues contentValues){
        return database.insert(DATABASE_TABLE, null, contentValues);
    }
    public int updateProvider(String id, ContentValues values){
        return database.update(DATABASE_TABLE, values, _ID + " =? ", new String[]{id});
    }
    public int deleteProvider(String id){
        return database.delete(DATABASE_TABLE, _ID + " =?", new String[]{id});

    }
}
